# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Server'
        db.create_table(u'terminals_server', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=255)),
            ('ip_address', self.gf('django.db.models.fields.IPAddressField')(default='', max_length=15, null=True, blank=True)),
        ))
        db.send_create_signal(u'terminals', ['Server'])

        # Adding field 'Terminal.server'
        db.add_column(u'terminals_terminal', 'server',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='terminals', null=True, to=orm['terminals.Server']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Server'
        db.delete_table(u'terminals_server')

        # Deleting field 'Terminal.server'
        db.delete_column(u'terminals_terminal', 'server_id')


    models = {
        u'terminals.operator': {
            'Meta': {'ordering': "['name']", 'object_name': 'Operator'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'})
        },
        u'terminals.server': {
            'Meta': {'ordering': "['name']", 'object_name': 'Server'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_address': ('django.db.models.fields.IPAddressField', [], {'default': "''", 'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'})
        },
        u'terminals.simcard': {
            'Meta': {'ordering': "['number']", 'object_name': 'SimCard'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'}),
            'operator': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'simcards'", 'to': u"orm['terminals.Operator']"})
        },
        u'terminals.terminal': {
            'Meta': {'ordering': "['number']", 'object_name': 'Terminal'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'terminals'", 'null': 'True', 'to': u"orm['terminals.Server']"}),
            'simcards': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'terminals'", 'blank': 'True', 'to': u"orm['terminals.SimCard']"}),
            't_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'terminals'", 'to': u"orm['terminals.TerminalType']"})
        },
        u'terminals.terminaltype': {
            'Meta': {'ordering': "['name']", 'object_name': 'TerminalType'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['terminals']