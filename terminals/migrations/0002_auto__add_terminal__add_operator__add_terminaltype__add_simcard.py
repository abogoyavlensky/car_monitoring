# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Terminal'
        db.create_table(u'terminals_terminal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('number', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=255)),
            ('t_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='terminals', to=orm['terminals.TerminalType'])),
        ))
        db.send_create_signal(u'terminals', ['Terminal'])

        # Adding M2M table for field simcards on 'Terminal'
        m2m_table_name = db.shorten_name(u'terminals_terminal_simcards')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('terminal', models.ForeignKey(orm[u'terminals.terminal'], null=False)),
            ('simcard', models.ForeignKey(orm[u'terminals.simcard'], null=False))
        ))
        db.create_unique(m2m_table_name, ['terminal_id', 'simcard_id'])

        # Adding model 'Operator'
        db.create_table(u'terminals_operator', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=255)),
        ))
        db.send_create_signal(u'terminals', ['Operator'])

        # Adding model 'TerminalType'
        db.create_table(u'terminals_terminaltype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=255)),
        ))
        db.send_create_signal(u'terminals', ['TerminalType'])

        # Adding model 'SimCard'
        db.create_table(u'terminals_simcard', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('number', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=255)),
            ('imei', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=255)),
            ('operator', self.gf('django.db.models.fields.related.ForeignKey')(related_name='simcards', to=orm['terminals.Operator'])),
        ))
        db.send_create_signal(u'terminals', ['SimCard'])


    def backwards(self, orm):
        # Deleting model 'Terminal'
        db.delete_table(u'terminals_terminal')

        # Removing M2M table for field simcards on 'Terminal'
        db.delete_table(db.shorten_name(u'terminals_terminal_simcards'))

        # Deleting model 'Operator'
        db.delete_table(u'terminals_operator')

        # Deleting model 'TerminalType'
        db.delete_table(u'terminals_terminaltype')

        # Deleting model 'SimCard'
        db.delete_table(u'terminals_simcard')


    models = {
        u'terminals.operator': {
            'Meta': {'ordering': "['name']", 'object_name': 'Operator'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'})
        },
        u'terminals.simcard': {
            'Meta': {'ordering': "['number']", 'object_name': 'SimCard'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'}),
            'operator': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'simcards'", 'to': u"orm['terminals.Operator']"})
        },
        u'terminals.terminal': {
            'Meta': {'ordering': "['number']", 'object_name': 'Terminal'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'}),
            'simcards': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'terminals'", 'blank': 'True', 'to': u"orm['terminals.SimCard']"}),
            't_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'terminals'", 'to': u"orm['terminals.TerminalType']"})
        },
        u'terminals.terminaltype': {
            'Meta': {'ordering': "['name']", 'object_name': 'TerminalType'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['terminals']