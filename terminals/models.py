# -*- coding: utf-8 -*-
__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel


class Operator(TimeStampedModel):
    name = models.CharField(default='', blank=True, null=True, max_length=255, verbose_name=_(u'имя оператора'))

    def __unicode__(self):
        return u'Имя оператора: {0}'.format(self.name)

    class Meta:
        ordering = ['name']
        verbose_name = _(u'оператор')
        verbose_name_plural = _(u'операторы')


class SimCard(TimeStampedModel):
    number = models.CharField(default='', blank=True, max_length=255, verbose_name=_(u'абонентский номер SIM-карты'))
    imei = models.CharField(default='', blank=True, max_length=255, verbose_name=_(u'IMEI SIM-карты'))
    operator = models.ForeignKey(Operator, related_name='simcards', verbose_name=_(u'оператор'))

    def __unicode__(self):
        return u'Абонетский номер: {0}'.format(self.number)

    class Meta:
        ordering = ['number']
        verbose_name = _(u'SIM-карта')
        verbose_name_plural = _(u'SIM-карты')


class TerminalType(TimeStampedModel):
    name = models.CharField(default='', blank=True, null=True, max_length=255, verbose_name=_(u'тип абонентского терминала'))

    def __unicode__(self):
        return u'тип терминала: {0}'.format(self.name)

    class Meta:
        ordering = ['name']
        verbose_name = _(u'тип абонентского терминала')
        verbose_name_plural = _(u'Типы абонентских терминалов')


class Server(TimeStampedModel):
    name = models.CharField(default='', blank=True, null=True, max_length=255, verbose_name=_(u'наименование сервера'))
    ip_address = models.IPAddressField(default='', blank=True, null=True, verbose_name=_(u'IP-адрес сервера'))

    def __unicode__(self):
        return u'наименование сервера: {0}'.format(self.name)

    class Meta:
        ordering = ['name']
        verbose_name = _(u'сервер')
        verbose_name_plural = _(u'серверы')


class Terminal(TimeStampedModel):
    number = models.CharField(default='', blank=False, null=False, unique=True, max_length=255, verbose_name=_(u'номер абонентского терминала'))
    t_type = models.ForeignKey(TerminalType, related_name='terminals', verbose_name=_(u'тип абонентского терминала'))
    simcards = models.ManyToManyField(SimCard, related_name='terminals', blank=True, verbose_name=_(u'SIM-карты'))
    server = models.ForeignKey(Server, related_name='terminals', blank=True, null=True, verbose_name=_(u'серверы'))

    def __unicode__(self):
        return u'Номер терминала: {0}'.format(self.number)

    class Meta:
        ordering = ['number']
        verbose_name = _(u'терминал')
        verbose_name_plural = _(u'терминалы')