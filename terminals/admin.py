from django.contrib import admin
from django.forms import ModelForm
from django.contrib.admin import ModelAdmin

from models import Terminal, TerminalType, SimCard, Operator, Server


class OperatorAdmin(ModelAdmin):
    search_fields = ('name', )
    list_per_page = 100


class SimCardAdmin(ModelAdmin):
    search_fields = ('number', 'imei', 'operator')
    list_per_page = 100


class TerminalTypeAdmin(ModelAdmin):
    search_fields = ('name', )
    list_per_page = 100


class ServerAdmin (ModelAdmin):
    search_fields = ('name', 'ip_address')
    list_display = ('name', 'ip_address')
    list_per_page = 100


class TerminalAdmin(ModelAdmin):
    search_fields = ('number', 't_type', 'simcards', 'server')
    list_display = ('number', 't_type', 'server')
    list_per_page = 100


admin.site.register(Operator, OperatorAdmin)
admin.site.register(SimCard, SimCardAdmin)
admin.site.register(TerminalType, TerminalTypeAdmin)
admin.site.register(Server, ServerAdmin)
admin.site.register(Terminal, TerminalAdmin)