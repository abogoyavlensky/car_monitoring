# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Fuel'
        db.create_table(u'cars_fuel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(related_name='fuels', to=orm['cars.Car'])),
            ('code', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('liters', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'cars', ['Fuel'])

        # Adding unique constraint on 'Fuel', fields ['code', 'car']
        db.create_unique(u'cars_fuel', ['code', 'car_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Fuel', fields ['code', 'car']
        db.delete_unique(u'cars_fuel', ['code', 'car_id'])

        # Deleting model 'Fuel'
        db.delete_table(u'cars_fuel')


    models = {
        u'cars.car': {
            'Meta': {'ordering': "['number']", 'object_name': 'Car'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cars'", 'to': u"orm['cars.CarModel']"}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '8'})
        },
        u'cars.carmodel': {
            'Meta': {'ordering': "['name']", 'object_name': 'CarModel'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'})
        },
        u'cars.fuel': {
            'Meta': {'ordering': "['code']", 'unique_together': "(('code', 'car'),)", 'object_name': 'Fuel'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fuels'", 'to': u"orm['cars.Car']"}),
            'code': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'liters': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'})
        }
    }

    complete_apps = ['cars']