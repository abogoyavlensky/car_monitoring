# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Car'
        db.create_table(u'cars_car', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('number', self.gf('django.db.models.fields.CharField')(default='', max_length=8)),
            ('model', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cars', to=orm['cars.CarModel'])),
        ))
        db.send_create_signal(u'cars', ['Car'])

        # Adding model 'CarModel'
        db.create_table(u'cars_carmodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=255)),
        ))
        db.send_create_signal(u'cars', ['CarModel'])


    def backwards(self, orm):
        # Deleting model 'Car'
        db.delete_table(u'cars_car')

        # Deleting model 'CarModel'
        db.delete_table(u'cars_carmodel')


    models = {
        u'cars.car': {
            'Meta': {'ordering': "['number']", 'object_name': 'Car'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cars'", 'to': u"orm['cars.CarModel']"}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '8'})
        },
        u'cars.carmodel': {
            'Meta': {'ordering': "['name']", 'object_name': 'CarModel'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['cars']