# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel


class CarModel(TimeStampedModel):
    name = models.CharField(_(u'наименование модели'), default='', blank=True, null=True, max_length=255)

    def __unicode__(self):
        return u'наименование модели: {0}'.format(self.name)

    class Meta:
        ordering = ['name']
        verbose_name = _(u'модель транспортного средства')
        verbose_name_plural = _(u'модели транспортных средств')


class Car(TimeStampedModel):
    number = models.CharField(_(u'гос. номер транспортного средство'), default='', blank=True, null=True, max_length=8)
    model = models.ForeignKey(CarModel, verbose_name=_(u'марка/модель автомобиля'), related_name='cars', blank=True, null=True)

    def __unicode__(self):
        return u'Гос. номер: {0}'.format(self.number)

    class Meta:
        ordering = ['number']
        verbose_name = _(u'транспортное средство')
        verbose_name_plural = _(u'транспортные средства')


class Fuel(TimeStampedModel):
    car = models.ForeignKey(Car, related_name='fuels', verbose_name=_(u'транспортное средство'))
    code = models.PositiveIntegerField(default=0, blank=False, verbose_name=_(u'код'))
    liters = models.PositiveIntegerField(default=0, blank=False, verbose_name=_(u'литры'))
    order = models.PositiveIntegerField(default=0, verbose_name=_(u'порядок расположения'))

    def __unicode__(self):
        return u'Код: {0}. Литры: {1}'.format(self.code, self.liters)

    class Meta:
        ordering = ['code']
        unique_together = ('code', 'car')
        verbose_name = _(u'расход топлива')
        verbose_name_plural = _(u'расход топлива')



