from django.contrib import admin
from django.forms import ModelForm
from django.contrib.admin import ModelAdmin

from models import Car, CarModel, Fuel
from suit.admin import SortableTabularInline


class CarModelAdmin(ModelAdmin):
    search_fields = ('name', )
    list_per_page = 100


class FuelAdmin(ModelAdmin):
    search_fields = ('code', 'liters', 'car')
    list_display = ('car', 'code', 'liters')
    list_per_page = 100


class FuelInline(SortableTabularInline):
    model = Fuel
    sortable = 'order'
    list_per_page = 100


class CarAdmin(ModelAdmin):
    inlines = (FuelInline, )
    search_fields = ('number', 'model')
    list_display = ('number', 'model')
    list_per_page = 100


admin.site.register(CarModel, CarModelAdmin)
admin.site.register(Car, CarAdmin)
admin.site.register(Fuel, FuelAdmin)