# -*- coding: utf-8 -*-
__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel

from cars.models import Car
from terminals.models import Terminal


class Client(TimeStampedModel):
    name = models.CharField(default='', blank=True, null=True, max_length=255, verbose_name=_(u'имя'))
    email = models.EmailField(default='', blank=True, verbose_name=_(u'адрес электронной почты'))
    phone = models.CharField(default='', blank=True, max_length=25, verbose_name=_(u'номер телефона'))
    person = models.CharField(default='', blank=True, max_length=25, verbose_name=_(u'контактное лицо'))

    def __unicode__(self):
        return u'Имя клиента: {0}'.format(self.name)

    class Meta:
        ordering = ['name']
        verbose_name = _(u'клиент')
        verbose_name_plural = _(u'клиенты')


class Place(TimeStampedModel):
    name = models.CharField(default='', blank=True, null=True, max_length=255, verbose_name=_(u'место проведения работ'))

    def __unicode__(self):
        return u'Место проведения работ: {0}'.format(self.name)

    class Meta:
        ordering = ['name']
        verbose_name = _(u'место проведения работ')
        verbose_name_plural = _(u'места проведения работ')


class Installer(TimeStampedModel):
    name = models.CharField(default='', blank=True, null=True, max_length=255, verbose_name=_(u'имя'))

    def __unicode__(self):
        return u'Имя монтажника: {0}'.format(self.name)

    class Meta:
        ordering = ['name']
        verbose_name = _(u'монтажник')
        verbose_name_plural = _(u'монтажники')


class Sensor(TimeStampedModel):
    name = models.CharField(default='', blank=True, null=True, max_length=255, verbose_name=_(u'наименование датчика'))

    def __unicode__(self):
        return u'Наименование датчика: {0}'.format(self.name)

    class Meta:
        ordering = ['name']
        verbose_name = _(u'датчик')
        verbose_name_plural = _(u'датчики')


class Installation(TimeStampedModel):
    number = models.CharField(_(u'номер акта'), default='', blank=True, max_length=200)
    client = models.ForeignKey(Client, related_name='installations', blank=True, null=True, verbose_name=_(u'наименование клиента'))
    car = models.ForeignKey(Car, related_name='installations', blank=True, null=True, verbose_name=_(u'транспортное средство'))
    terminal = models.ForeignKey(Terminal, related_name='installations', blank=True, null=True, verbose_name=_(u'терминал'))
    installer = models.ForeignKey(Installer, related_name='installations', blank=True, null=True, verbose_name=_(u'монтажник'))
    place = models.ForeignKey(Place, related_name='installations', blank=True, null=True, verbose_name=_(u'место проведения работ'))
    sensors = models.ManyToManyField(Sensor, related_name='installations', blank=True, null=True, verbose_name=_(u'датчики'))
    date_installation = models.DateTimeField(default=timezone.now, blank=True, null=True, verbose_name=_(u'дата монтажа'))
    date_simcard = models.DateTimeField(blank=True, null=True, verbose_name=_(u'дата установки SIM-карты'))

    def __unicode__(self):
        return u'Дата: {0}. Клиент: {1}. Транспорт: {2}'.format(self.date_installation, self.client, self.car)

    class Meta:
        ordering = ['-date_installation']
        verbose_name = _(u'инсталяция')
        verbose_name_plural = _(u'инсталяции')


class InstallationHistory(TimeStampedModel):
    #TODO: refactor as dynamic generated fields from Installation model
    INSTALLATION_FIELDS = (
        ('client', _('Client')),
        ('car', _('Car')),
    )

    user = models.ForeignKey(get_user_model(), related_name='histories', blank=False, null=False)
    installation = models.ForeignKey(Client, related_name='histories', blank=False, null=False)
    fields = models.CharField(max_length=16, choices=INSTALLATION_FIELDS, default='', blank=True, null=True)