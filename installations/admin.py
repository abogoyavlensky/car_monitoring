# -*- coding: utf-8 -*-
from django.template.defaulttags import now

import os
import xlrd
import datetime

from django.conf import settings
from import_export import fields
from django.contrib import admin, messages
from django.contrib.admin import ModelAdmin
from import_export.admin import ImportExportModelAdmin
from django.utils.translation import ugettext_lazy as _
from import_export import resources, results, widgets, formats

from reportlab.pdfbase import pdfmetrics
from reportlab.lib import colors, styles
from reportlab.pdfbase.ttfonts import TTFont
from django.core.urlresolvers import reverse
from reportlab.lib.pagesizes import inch, A4
from suit.widgets import SuitSplitDateTimeWidget
from django.http import HttpResponseRedirect, HttpResponse
from reportlab.platypus import SimpleDocTemplate, Table, Paragraph, PageBreak, Image

from cars.models import Car, CarModel
from terminals.models import Operator, SimCard, TerminalType, Terminal, Server
from models import Client, Installation, Installer, InstallationHistory, Sensor, Place

CURRENT_PATH = os.path.dirname(__file__)
CURRENT_FONT = 'Verdana'
font_path = os.path.join(settings.PROJECT_ROOT, 'core/static/fonts/verdana.ttf')
pdfmetrics.registerFont(TTFont(CURRENT_FONT, font_path))

FONTSIZE_NORMAL = 8
FONTSIZE_EXTRA = 10


def make_report(modeladmin, request, queryset):
    response = HttpResponse(mimetype='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=Installations__{0}__{1}.pdf'.format('_'.join(list(str(i) for i in queryset.order_by('pk').values_list('pk', flat=True))),
                                                                                              datetime.date.today().strftime("%d-%m-%Y"))

    doc = SimpleDocTemplate(response, rightMargin=1, leftMargin=20, topMargin=10, bottomMargin=10)
    elements = []
    elements.append(p(''))

    for obj in queryset:
        data = [[u'Акт', u'№', '', u'от', datetime.date.today().strftime("%d.%m.%Y")],
                [u'На установку и пуско-наладку абонентского терминала и дополнительного оборудования.', '', '', '', ''],
                [u'Исполнитель: ООО «Глонасссофт-55»', '', '', '', ''],
                [u'в лице техника-установщика:', obj.installer.name, '', '', ''],
                [u'Заказчик (полное наименование)', obj.client.name, '', '', ''],
                [u'в лице:', obj.client.person, '', '', ''],
                [u'Место проведения работ', u'Территория ГП Кормиловское ДРСУ', '', '', ''],
                [u'1. Данные о транспортном средстве, на котором установлен АТ и произведены работы:', '', '', '', ''],
                [u'Наименование (марка/модель) ТС:', obj.car.model.name, '', '', ''],
                [u'Государственный регистрационный знак:', obj.car.number, '', '', ''],
                [u'2. Оборудование, установленное на ТС', '', '', '', ''],
                [u'Тип устанавливаемого абонентского терминала:', obj.terminal.t_type.name, '', '', ''],
                [u'Серийный номер АТ:', obj.terminal.number, '', '', ''],
                [u'Штатное опломбирование корпуса АТ (наклейка)', '', '', u'[ ]ДА', u'[ ]НЕТ'],
                [u'Опломбирование разъемов интерфейсного кабеля АТ (наклейка)', '', '', u'[ ]ДА', u'[ ]НЕТ'],    #14 line
        ]

        table_style = [('FONT', (0, 0), (-1, -1), CURRENT_FONT),
                               ('GRID', (0, 0), (-1, -1), 0.7, colors.black),
                               ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                               ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                               ('FONTSIZE', (0, 0), (-1, -1), FONTSIZE_NORMAL),

                               ('FONTSIZE', (0, 0), (4, 2), FONTSIZE_EXTRA),
                               ('SPAN', (1, 0), (2, 0)),
                               ('SPAN', (0, 1), (4, 1)),
                               ('SPAN', (0, 2), (4, 2)),
                               ('SPAN', (1, 3), (4, 3)),

                               ('FONTSIZE', (0, 4), (4, 4), FONTSIZE_EXTRA),
                               ('SPAN', (1, 4), (4, 4)),

                               ('SPAN', (1, 5), (4, 5)),

                               ('FONTSIZE', (0, 6), (4, 7), FONTSIZE_EXTRA),
                               ('SPAN', (1, 6), (4, 6)),
                               ('SPAN', (0, 7), (4, 7)),

                               ('SPAN', (1, 8), (4, 8)),
                               ('SPAN', (1, 9), (4, 9)),

                               ('FONTSIZE', (0, 10), (4, 10), FONTSIZE_EXTRA),
                               ('SPAN', (0, 10), (4, 10)),

                               ('FONTSIZE', (1, 11), (1, 11), FONTSIZE_EXTRA),
                               ('SPAN', (1, 11), (4, 11)),

                               ('SPAN', (1, 12), (4, 12)),

                               ('SPAN', (0, 13), (2, 13)),
                               ('SPAN', (0, 14), (2, 14)),    #14 line
        ]

        row_index = 15
        for n, sim in enumerate(obj.terminal.simcards.all()):
            data.extend([[u'Номер SIM-карты', sim.number, '', '', ''],
                         [u'IMEI SIM-карты', sim.imei, '', '', ''],
                         [u'Оператор сотовой связи', sim.operator.name, '', '', ''],
            ])

            for i in xrange(3):
                table_style.append(('SPAN', (1, row_index), (4, row_index)))
                row_index += 1

        data.extend([
            [u'Место размещения основного блока и подключения АТ в ТС:', '', '', '', ''],
            [u'Размещение ГЛОНАСС/GPS антенны:', '', '', '', ''],
            [u'Размещение GSM антенны:', '', '', '', ''],
            [u'Подключение питания с отключением массы:', '', '', u'[ ]ДА', u'[ ]НЕТ'],
            [u'2.1 Дополнительные внешние устройства и датчики, установленные на ТС:', '', '', '', ''],
            [u'датчик уровня топлива ( кол - во шт. )', obj.sensors.all().count(), '', '', ''],
        ])

        table_style.extend([
            ('SPAN', (1, row_index), (4, row_index)),
            ('SPAN', (1, row_index + 1), (4, row_index + 1)),
            ('SPAN', (1, row_index + 2), (4, row_index + 2)),
            ('SPAN', (0, row_index + 3), (2, row_index + 3)),

            ('FONTSIZE', (0, row_index + 4), (4, row_index + 4), FONTSIZE_EXTRA),
            ('SPAN', (0, row_index + 4), (4, row_index + 4)),

            ('SPAN', (1, row_index + 5), (4, row_index + 5)),
        ])

        row_index += 6
        for n, sensor in enumerate(obj.sensors.all()):
            data.extend([[sensor.name, u'Серийный номер', '', '', ''],
                         ['', u'Номер пломбы 1 (корпус ДУТ)' '', '', ''],
                         ['', u'Номер пломбы 2 (разъем )', '', '', ''],
            ])

            for i in xrange(3):
                table_style.append(('SPAN', (2, row_index), (4, row_index)))
                row_index += 1

        I = Image(os.path.join(settings.BASE_DIR, 'core/static/images/report_bottom_line.jpg'))
        I.drawHeight = 0.4*inch
        I.drawWidth = 7.5*inch

        data.extend([
            [u'реле включения зажигания (шт.)', '', '', '', ''],
            [u'датчик навесного оборудования (шт.)', '', '', '', ''],
            [u'проведена тарировка установленных датчиков уровня топлива (кол-во шт.)', '', '', '', ''],
            [u'представитель Заказчика получил тарировочные таблицы (кол-во шт.)', '', '', '', ''],
            [u'Представитель Заказчика подтверждает, что:', '', '', '', ''],
            [u'- принимает вышеуказанные работы, которые были проведены на данном транспортном сре дстве;', '', '', '', ''],
            [u'- транспортное средство находится в исправном состоянии;', '', '', '', ''],
            [u'- установленное оборудование не внесло изменений в функциональность ТС;', '', '', '', ''],
            [u'- оборудование принято Заказчиком в эксплуатацию;', '', '', '', ''],
            [u'- претензий к функционированию бортового оборудования не имеет.', '', '', '', ''],

            [u'- работоспособность оборудования в части взаимодействия с клиентским ПО системы проверена', '', '', '', ''],
            ['', '', '', '', ''],
            [u'От Исполнителя:', u'Ф.И.О. Ли В.Л.', '', u'Подпись', ''],
            [u'От Заказчика:', u'Ф.И.О.', '', u'Подпись', ''],
        ])

        table_style.extend([
            ('SPAN', (1, row_index), (4, row_index)),
            ('SPAN', (1, row_index + 1), (4, row_index + 1)),

            ('SPAN', (0, row_index + 2), (1, row_index + 2)),
            ('SPAN', (2, row_index + 2), (4, row_index + 2)),

            ('SPAN', (0, row_index + 3), (1, row_index + 3)),
            ('SPAN', (2, row_index + 3), (4, row_index + 3)),

            ('FONTSIZE', (0, row_index + 4), (4, row_index + 4), FONTSIZE_EXTRA),
            ('SPAN', (0, row_index + 4), (4, row_index + 4)),

            ('SPAN', (0, row_index + 5), (4, row_index + 5)),
            ('SPAN', (0, row_index + 6), (4, row_index + 6)),
            ('SPAN', (0, row_index + 7), (4, row_index + 7)),
            ('SPAN', (0, row_index + 8), (4, row_index + 8)),
            ('SPAN', (0, row_index + 9), (4, row_index + 9)),

            ('SPAN', (0, row_index + 10), (2, row_index + 11)),

            ('SPAN', (1, row_index + 12), (2, row_index + 12)),
            ('SPAN', (1, row_index + 13), (2, row_index + 13)),
        ])


        t = Table(data, style=table_style, rowHeights=15)
        elements.extend([t,I, PageBreak()])

    doc.pagesize = A4
    doc.build(elements)
    return response

make_report.short_description = _(u"Печатать отчет для выбранных инсталяций")


def p(text):
    style_sheet = styles.getSampleStyleSheet()
    style_sheet.add(styles.ParagraphStyle(name='MyStyle',
                                          fontName='Verdana',
                                          fontSize=11,
                                          leading=11))
    s = '<b>{0}</b>'.format(text)
    return Paragraph(s, style_sheet["MyStyle"])


class ClientAdmin(ModelAdmin):
    search_fields = ('name', )
    list_per_page = 100

class InstallerAdmin(ModelAdmin):
    search_fields = ('name', )
    list_per_page = 100

class SensorAdmin(ModelAdmin):
    search_fields = ('name', )
    list_per_page = 100


class DateTimeWidgetXLS(widgets.DateWidget):

    def clean(self, value):
        if not value:
            return None
        return datetime.datetime(*xlrd.xldate_as_tuple(value, 0))


class CharWidgetXLS(widgets.DateWidget):

    def clean(self, value):
        if isinstance(value, basestring):
            return value
        else:
            return str(value.as_integer_ratio()[0])


class InstallationResource(resources.Resource):
    company_name = fields.Field(column_name=u'Наименование предприятия', widget=CharWidgetXLS())
    place_name = fields.Field(column_name=u'Место проведения работ', widget=CharWidgetXLS())
    car_number = fields.Field(column_name=u'Гос. номер транспортного средства', widget=CharWidgetXLS())
    terminal_number = fields.Field(column_name=u'Номер абонентского терминала', widget=CharWidgetXLS())
    date_terminal_installation = fields.Field(column_name=u'Дата установки абонентского терминала', widget=DateTimeWidgetXLS())
    terminal_type = fields.Field(column_name=u'Тип абонентского терминала', widget=CharWidgetXLS())
    car_type = fields.Field(column_name=u'Тип автомобиля', widget=CharWidgetXLS())
    simcard_number = fields.Field(column_name=u'Абонентский номер сим-карты', widget=CharWidgetXLS())
    simcard_imei= fields.Field(column_name=u'IMEI сим-карты', widget=CharWidgetXLS())
    operator_name = fields.Field(column_name=u'Оператор сим-карты', widget=CharWidgetXLS())
    date_simcard_installation = fields.Field(column_name=u'Дата установки сим-карты', widget=DateTimeWidgetXLS())
    installer_name = fields.Field(column_name=u'Монтажник', widget=CharWidgetXLS())
    sensors_name = fields.Field(column_name=u'Установленные датчики (тип датчика, № входа, тип подключения)', widget=CharWidgetXLS())
    report_number = fields.Field(column_name=u'Номер акта', widget=CharWidgetXLS())
    server_name = fields.Field(column_name=u'Сервер', widget=CharWidgetXLS())

    def import_data(self, dataset, dry_run=True, raise_errors=False,
            use_transactions=None):
        result = results.Result()
        for i, row in enumerate(dataset.dict):
            corrected_row = self.get_diff_headers_2(row)
            if corrected_row:

                if i == 188:
                    k = 0

                row_result = results.RowResult()
                result.rows.append(self.validate_row(corrected_row, row_result))

                car_model, created = CarModel.objects.get_or_create(name=row_result.car_type.strip() if row_result.car_type else '')
                if row_result.car_number.strip() == u'без номера' or not row_result.car_number:
                    row_result.car_number = ''
                car, created = Car.objects.get_or_create(number=row_result.car_number.strip().split('\\')[0], model=car_model)

                operator, created = Operator.objects.get_or_create(name=row_result.operator_name.strip() if row_result.operator_name else '')

                # simcards_n = row_result.simcard_number.split(';')
                simcards_n, simcards_i = [], []
                for ii, sim in enumerate(row_result.simcard_number.split(';')):
                    if sim.isdigit():
                        simcards_n.append(sim)
                for ii, sim in enumerate(row_result.simcard_imei.split(';')):
                    if sim.isdigit():
                        simcards_i.append(sim)
                simcards = []
                for j, val in enumerate(simcards_n):
                    if val.isdigit():
                        simcard, created = SimCard.objects.get_or_create(number=simcards_n[j].strip(),
                                                                         defaults={
                                                                             'imei': simcards_i[j].strip() if len(simcards_i) > j else '',
                                                                             'operator': operator
                                                                         })
                        # simcard.imei = simcards_i[j].strip() if len(simcards_i) > j else ''
                        # simcard.save()
                        simcards.append(simcard)

                terminal_type, created = TerminalType.objects.get_or_create(name=row_result.terminal_type.strip() if row_result.terminal_type else '')

                server, created = Server.objects.get_or_create(name=row_result.server_name.strip() if row_result.server_name else '')
                if row_result.terminal_number:
                    terminal, created = Terminal.objects.get_or_create(number=row_result.terminal_number.strip(),
                                                                       defaults={'t_type': terminal_type,
                                                                                 'server': server})
                    if created:
                        terminal.simcards = simcards
                    else:
                        terminal.simcards.add(*simcards)
                    terminal.save()
                else:
                    terminal = None

                client, created = Client.objects.get_or_create(name=row_result.company_name.strip() if row_result.company_name else '')
                place, created = Place.objects.get_or_create(name=row_result.place_name.strip() if row_result.place_name else '')
                installer, created = Installer.objects.get_or_create(name=row_result.installer_name.strip() if row_result.installer_name else '')
                sensors, created = Sensor.objects.get_or_create(name=row_result.sensors_name.strip() if row_result.sensors_name else '')
                installation, created = Installation.objects.get_or_create(number=row_result.report_number.strip(),
                                                                           client=client,
                                                                           car=car,
                                                                           terminal=terminal,
                                                                           installer=installer,
                                                                           place=place,
                                                                           date_installation=row_result.date_terminal_installation,
                                                                           date_simcard=row_result.date_simcard_installation,
                )
                if sensors:
                    installation.sensors.add(sensors)
                    installation.save()
        return result

    def validate_field(self, name, field, data, obj):
        field.attribute = name
        field.save(obj, data)

    def validate_row(self, row, row_result):
        for name in self.fields:
            self.validate_field(name, self.fields.get(name), row, row_result)
        return row_result

    def get_diff_headers_2(self, row):
        fields_headers = super(InstallationResource, self).get_diff_headers()
        for header in row.keys():
            if header and header.strip() in fields_headers:
                value = row.pop(header)
                row[header.strip()] = value
            elif header:
                return "You have extra header with name {0} in source table".format(header)
        return row


class InstallationAdmin(ImportExportModelAdmin):
    resource_class = InstallationResource
    list_display = ('client', 'car', 'terminal', 'installer', 'date_installation', 'date_simcard')
    search_fields = ('client', 'car', 'terminal', 'installer', 'date_installation', 'date_simcard')
    actions = [make_report]
    list_per_page = 100

    def get_import_formats(self):
        """
        Returns available import formats.
        """
        return [formats.base_formats.XLS, ]

    def import_action(self, request, *args, **kwargs):
        result = super(InstallationAdmin, self).import_action(request, *args, **kwargs)
        if request.method == "POST":
            opts = self.model._meta
            success_message = _('Import finished')
            messages.success(request, success_message)
            url = reverse('admin:%s_%s_changelist' %
                          (opts.app_label, opts.module_name),
                          current_app=self.admin_site.name)
            return HttpResponseRedirect(url)
        else:
            return result

    class Meta:
        widgets = {
            'date_installation': SuitSplitDateTimeWidget,
            'date_simcard': SuitSplitDateTimeWidget
        }


class InstallationHistoryAdmin(ModelAdmin):
    list_display = ('user', 'installation', 'fields')
    search_fields = ('user', 'installation', 'fields')


admin.site.register(Client, ClientAdmin)
admin.site.register(Installer, InstallerAdmin)
admin.site.register(Sensor, SensorAdmin)
admin.site.register(Installation, InstallationAdmin)
admin.site.register(InstallationHistory, InstallationHistoryAdmin)